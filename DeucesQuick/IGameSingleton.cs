﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VideoPokerCore;

namespace DeucesQuick
{
    public interface IGameSingleton
    {

        Dictionary<string, Game> ActiveGames { get; set; }
    }
}
