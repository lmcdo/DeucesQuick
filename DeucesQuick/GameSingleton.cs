﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VideoPokerCore;

namespace DeucesQuick
{
    public class GameSingleton : IGameSingleton
    {

        private Dictionary<string, Game> activeGames = new Dictionary<string, Game>();
        public Dictionary<string, Game> ActiveGames { get
            {
                return activeGames;
            }
            set
            {
                activeGames = value;
            }
        }
    }
}
