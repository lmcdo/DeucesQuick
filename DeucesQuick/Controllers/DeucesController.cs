﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VideoPokerCore;

namespace DeucesQuick.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeucesController : ControllerBase
    {
        private readonly IGameSingleton _singleton;

        [HttpGet("set/{data}")]
        public IActionResult BeginHand(int data)
        {
            Game game;
            if (HttpContext.Session.GetInt32("last_event") == 0)
            {
                return BadRequest();
            }
            HttpContext.Session.SetInt32("last_event", 0);
            HttpContext.Session.SetInt32("bet", data);
            if (!_singleton.ActiveGames.TryGetValue(HttpContext.Session.Id, out game))
            {
                game = new Game(new DeucesDefaultTable(), new ThemedString((inSuit, inRank, muhstring) =>
                {
                    return "https://lmcdo.com/deuceswild/cards/c" + ((int)inSuit * 13 + (int)inRank) + ".svg";
                }), "White", "Purple", new Random());
                game.Bet();
                _singleton.ActiveGames.Add(HttpContext.Session.Id, game);

                HttpContext.Session.SetInt32("tokens", 1000 - data);
            }
            else
            {
                HttpContext.Session.SetInt32("tokens", HttpContext.Session.GetInt32("tokens").Value - data);
            }
            
            game.Bet();
            return Ok(game.Hand.Select(card => card.CardString));
        }


        [HttpGet("get")]
        public IActionResult getsessiondata()
        {
            var sessionData = HttpContext.Session.GetInt32("bet");
            return Ok(sessionData);
        }

        [HttpGet("keep")]
        public IActionResult KeepCards(List<bool> toKeep)
        {
            Game game;
            if (HttpContext.Session.GetInt32("last_event") == 1)
            {
                return BadRequest();
            }
            HttpContext.Session.SetInt32("last_event", 1);
            if (!_singleton.ActiveGames.TryGetValue(HttpContext.Session.Id, out game))
            {
                return BadRequest();
            }

            HttpContext.Session.SetInt32("tokens", HttpContext.Session.GetInt32("tokens").Value + (HttpContext.Session.GetInt32("bet").Value * game.Score));
            return Ok();
        }

        [HttpDelete]
        public IActionResult EndSession()
        {
            if (_singleton.ActiveGames.Remove(HttpContext.Session.Id))
                return Ok();
            else return BadRequest();
        }

        public DeucesController(IGameSingleton singleton)
        {
            _singleton = singleton;
        }
    }
}
